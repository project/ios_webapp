Drupal.behaviors.ios_webapp = function(context){
  if (("standalone" in window.navigator) && window.navigator.standalone) {
    $("a").click(function (event) {
      event.preventDefault();
      window.location = $(this).attr("href");
    });
  }
};
