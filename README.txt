-- SUMMARY --
The Safari web browser supports several meta tags and links that allow you to 
customize how Safari appears when displaying your page from a bookmark on
your iOS device's homescreen.  Embedding these tags and links into your HTML
can cause Safari to behave in several beneficial ways.  For example, you can 
launch your website in full screen mode so it appears to be an app instead of
a website.  

You can also specify an image to use as the icon for the bookmark on your
homescreen.

-- REQUIREMENTS --
This module requires the Filefield module.  This project can be found at 
https://drupal.org/project/filefield.
If the Prevent links from opening in a new window check box is checked in the
configuration for the module, then javascript must be enabled. If javascript
is not enabled, then checking this check box will not have the intended
results.

-- INSTALLATION --
Install as usual, see http://drupal.org/node/70151 for further information.
 
-- CONFIGURATION --
To configure this module, navigate to the path admin/settings/ios_webapp
(Site Configuration -> iOS Webapp module settings).

-- SUPPORTED META TAGS --
apple-mobile-web-app-capable

-- SUPPORTED LINK TAGS --
apple-touch-icon (sizes 57x57, 72x72, 114x114, 144x144)

-- ADDITIONAL NOTES --
At the top of the settings page, I included a checkbox "Prevent links from
opening in a new window".  If a webpage is running on an iOS Device as a 
web app, then links will be opened in a new window by default.  Checking
this checkbox allows the user to prevent this behavior.  This is done by
including the /js/ios_webapp.js file when this checkbox is checked.  This
functionality has been tested on iOS 6.1.

-- TODO --
Add support for the following meta tags:
* apple-mobile-web-app-status-bar-style
* format-detection
* viewport

Add support for the following link tags
* apple-touch-startup-image

Verify the "Prevent links from opening in a new window" functionality works
in earlier versions of iOS (less than 6.1)

See The Supported Mega Tags and the Configuring Web
Applications sections of Apple's Developer website for more information.
