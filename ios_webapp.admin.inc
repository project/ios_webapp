<?php
/**
 * @file
 * Define administration form for the ios_webapp module.
 */

/**
 * Form constructor for the ios_webapp admin form.
 *
 * @see ios_webapp_admin_for_files_submit()
 *
 * @ingroup forms
 */
function ios_webapp_admin() {
  $form = array();

  $form['ios_webapp_prevent_links_new_window'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prevent links from openining in new window'),
    '#description' => t('Check this checkbox to prevent links from opening in Safari'),
    '#default_value' => variable_get('ios_webapp_prevent_links_new_window', 0),
  );

  // Set up fieldset and fields for the iPhone/iPod app icon (57px x 57px)
  $form['ios_webapp_iphone_icon_fs'] = array(
    '#type' => 'fieldset',
    '#title' => t('iPhone web app icon'),
  );
  $iphone_icon_fid = variable_get('ios_webapp_iphone_icon', '');
  $iphone_markup = "";
  if (strlen($iphone_icon_fid) > 0) {
    $iphone_icon = ios_webapp_file_load($iphone_icon_fid);
    $iphone_markup .= t('Upload a picture to be used as the icon when this site is saved on the home screen.  The size should be 57 pixels by 57 pixels.') . "<br/>";
    $iphone_markup .= theme('image', $iphone_icon->filepath) . "<br/>";
  }
  else {
    $iphone_markup .= t("An icon has not been specified for an iPhone web app.");
  }
  $form['ios_webapp_iphone_icon_fs']['ios_webapp_iphone_cur_icon'] = array(
    '#type' => 'markup',
    '#value' => $iphone_markup,
  );
  $form['ios_webapp_iphone_icon_fs']['ios_webapp_iphone_icon_upload'] = array(
    '#type' => 'file',
    '#title' => t('Use a different icon'),
  );
  $form['ios_webapp_iphone_icon_fs']['ios_webapp_iphone_icon_remove'] = array(
    '#type' => 'submit',
    '#name' => 'ios_webapp_iphone_icon_remove',
    '#value' => t('Remove'),
  );

  // Set up fieldset and fields for the iPhone/iPod retina icon (114px x 114px)
  $form['ios_webapp_iphone_retina_icon_fs'] = array(
    '#type' => 'fieldset',
    '#title' => t('iPhone with retina display web app icon'),
  );
  $iphone_retina_icon_fid = variable_get('ios_webapp_iphone_retina_icon', '');
  $iphone_retina_markup = "";
  if (strlen($iphone_retina_icon_fid) > 0) {
    $iphone_retina_icon = ios_webapp_file_load($iphone_retina_icon_fid);
    $iphone_retina_markup .= t('Upload a picture to be used as the icon when this site is saved on the home screen.  The size should be 114 pixels by 114 pixels.') . "<br/>";
    $iphone_retina_markup .= theme('image', $iphone_retina_icon->filepath) . "<br/>";
  }
  else {
    $iphone_retina_markup .= t("An icon has not been specified for an iPhone with retina display web app.");
  }
  $form['ios_webapp_iphone_retina_icon_fs']['ios_webapp_iphone_retina_cur_icon'] = array(
    '#type' => 'markup',
    '#value' => $iphone_retina_markup,
  );
  $form['ios_webapp_iphone_retina_icon_fs']['ios_webapp_iphone_retina_icon_upload'] = array(
    '#type' => 'file',
    '#title' => t('Use a different icon'),
  );
  $form['ios_webapp_iphone_retina_icon_fs']['ios_webapp_iphone_retina_icon_remove'] = array(
    '#type' => 'submit',
    '#name' => 'ios_webapp_iphone_retina_icon_remove',
    '#value' => t('Remove'),
  );

  // Set up fieldset and fields for the iPad app icon (72px x 72px)
  $form['ios_webapp_ipad_icon_fs'] = array(
    '#type' => 'fieldset',
    '#title' => t('iPad web app icon'),
  );
  $ipad_icon_fid = variable_get('ios_webapp_ipad_icon', '');
  $markup = "";
  if (strlen($ipad_icon_fid) > 0) {
    $file = ios_webapp_file_load($ipad_icon_fid);
    $markup .= t('Upload a picture to be used as the icon when this site is saved on the home screen.  The size should be 72 pixels by 72 pixels.') . "<br/>";
    $markup .= theme('image', $file->filepath) . "<br/>";
  }
  else {
    $markup .= t("An icon has not been specified for an iPad web app.");
  }
  $form['ios_webapp_ipad_icon_fs']['ios_webapp_cur_icon'] = array(
    '#type' => 'markup',
    '#value' => $markup,
  );
  $form['ios_webapp_ipad_icon_fs']['ios_webapp_icon_upload'] = array(
    '#type' => 'file',
    '#title' => t('Use a different icon'),
  );
  $form['ios_webapp_ipad_icon_fs']['ios_webapp_ipad_icon_remove'] = array(
    '#type' => 'submit',
    '#name' => 'ios_webapp_ipad_icon_remove',
    '#value' => t('Remove'),
  );

  // Set up fieldset and fields for the iPad retina icon (144px x 144px)
  $form['ios_webapp_ipad_retina_icon_fs'] = array(
    '#type' => 'fieldset',
    '#title' => t('iPad with retina display web app icon'),
  );
  $ipad_retina_icon_fid = variable_get('ios_webapp_ipad_retina_icon', '');
  $ipad_retina_markup = "";
  if (strlen($ipad_retina_icon_fid) > 0) {
    $ipad_retina_icon = ios_webapp_file_load($ipad_retina_icon_fid);
    $ipad_retina_markup .= t('Upload a picture to be used as the icon when this site is saved on the home screen of an iPad with retina display.<br/>');
    $ipad_retina_markup .= t('The size should be 144 pixels by 144 pixels.<br/>');
    $ipad_retina_markup .= theme('image', $ipad_retina_icon->filepath) . "<br/>";
  }
  else {
    $ipad_retina_markup .= t("An icon has not been specified for an iPad with retina display web app.");
  }
  $form['ios_webapp_ipad_retina_icon_fs']['ios_ipad_retina_cur_icon'] = array(
    '#type' => 'markup',
    '#value' => $ipad_retina_markup,
  );
  $form['ios_webapp_ipad_retina_icon_fs']['ios_webapp_ipad_retina_icon_upload'] = array(
    '#type' => 'file',
    '#title' => t('Use a different icon'),
  );
  $form['ios_webapp_ipad_retina_icon_fs']['ios_webapp_ipad_retina_icon_remove'] = array(
    '#type' => 'submit',
    '#name' => 'ios_webapp_ipad_retina_icon_remove',
    '#value' => t('Remove'),
  );

  // Custom form attributes.
  $form['#submit'][] = 'ios_webapp_admin_for_files_submit';
  $form['#attributes']['enctype'] = 'multipart/form-data';

  return system_settings_form($form);
}

/**
 * Form submission handler for ios_webapp_admin().
 *
 * @see ios_webapp_admin()
 */
function ios_webapp_admin_for_files_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'ios_webapp_iphone_icon_remove') {
    variable_del('ios_webapp_iphone_icon');
  }
  if ($form_state['clicked_button']['#name'] == 'ios_webapp_iphone_retina_icon_remove') {
    variable_del('ios_webapp_iphone_retina_icon');
  }
  if ($form_state['clicked_button']['#name'] == 'ios_webapp_ipad_icon_remove') {
    variable_del('ios_webapp_ipad_icon');
  }
  if ($form_state['clicked_button']['#name'] == 'ios_webapp_ipad_retina_icon_remove') {
    variable_del('ios_webapp_ipad_retina_icon');
  }

  if ($iphone_file = file_save_upload('ios_webapp_iphone_icon_upload', array(), file_directory_path(), FILE_EXISTS_RENAME)) {
    $iphone_file->status = FILE_STATUS_PERMANENT;
    $update_iphone_file = "fid";
    drupal_write_record('files', $iphone_file, $update_iphone_file);

    // If one is currently set, flag it for removal.
    $cur_iphone_icon_fid = variable_get('ios_webapp_iphone_icon', 0);
    if ($cur_iphone_icon_fid > 0) {
      $cur_iphone_icon_file = ios_webapp_file_load($cur_iphone_icon_fid);
      $cur_iphone_icon_file->status = FILE_STATUS_TEMPORARY;
      $update_cur_iphone_icon_file = "fid";
      drupal_write_record('files', $cur_iphone_icon_file, $update_cur_iphone_icon_file);
    }
    variable_set('ios_webapp_iphone_icon', $iphone_file->fid);
  }

  if ($iphone_retina_file = file_save_upload('ios_webapp_iphone_retina_icon_upload', array(), file_directory_path(), FILE_EXISTS_RENAME)) {
    $iphone_retina_file->status = FILE_STATUS_PERMANENT;
    $update_iphone_retina_file = "fid";
    drupal_write_record('files', $iphone_retina_file, $update_iphone_retina_file);

    // If one is currently set, flag it for removal.
    $cur_iphone_retina_icon_fid = variable_get('ios_webapp_iphone_retina_icon', 0);
    if ($cur_iphone_retina_icon_fid > 0) {
      $cur_iphone_retina_icon_file = ios_webapp_file_load($cur_iphone_retina_icon_fid);
      $cur_iphone_retina_icon_file->status = FILE_STATUS_TEMPORARY;
      $update_cur_iphone_retina_icon_file = "fid";
      drupal_write_record('files', $cur_iphone_retina_icon_file, $update_cur_iphone_retina_icon_file);
    }
    variable_set('ios_webapp_iphone_retina_icon', $iphone_retina_file->fid);
  }

  // Set up new and remove old iPad web app icon.
  if ($file = file_save_upload('ios_webapp_icon_upload', array(), file_directory_path(), FILE_EXISTS_RENAME)) {
    $file->status = FILE_STATUS_PERMANENT;
    $update_file = "fid";
    drupal_write_record('files', $file, $update_file);

    // If one is currently set, flag it for removal.
    $cur_icon_fid = variable_get('ios_webapp_ipad_icon', 0);
    if ($cur_icon_fid > 0) {
      $cur_file = ios_webapp_file_load($cur_icon_fid);
      $cur_file->status = FILE_STATUS_TEMPORARY;
      $update_cur_file = "fid";
      drupal_write_record('files', $cur_file, $update_cur_file);
    }
    variable_set('ios_webapp_ipad_icon', $file->fid);
  }

  // Set up new and remove old iPad retina web app icon.
  if ($ipad_retina_file = file_save_upload('ios_webapp_ipad_retina_icon_upload', array(), file_directory_path(), FILE_EXISTS_RENAME)) {
    $ipad_retina_file->status = FILE_STATUS_PERMANENT;
    $update_ipad_retina_file = "fid";
    drupal_write_record('files', $ipad_retina_file, $update_ipad_retina_file);

    // If one is currently set, flag it for removal.
    $cur_ipad_retina_icon_fid = variable_get('ios_webapp_ipad_retina_icon', 0);
    if ($cur_ipad_retina_icon_fid > 0) {
      $cur_ipad_retina_file = ios_webapp_file_load($cur_ipad_retina_icon_fid);
      $cur_ipad_retina_file->status = FILE_STATUS_TEMPORARY;
      $update_cur_ipad_retina_file = "fid";
      drupal_write_record('files', $cur_ipad_retina_file, $update_cur_ipad_retina_file);
    }
    variable_set('ios_webapp_ipad_retina_icon', $ipad_retina_file->fid);
  }
}
